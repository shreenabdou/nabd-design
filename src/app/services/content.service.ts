import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContentService {

  private urlDomain = environment.apiURI;
  public params = {
    "ContentTypes": [],
    "TitleAr": "",
    "TitleEn": "",
    "BodyAr": "",
    "BodyEn": "",
    "Type": "",
    "Active": false,
    "Acsess": false,
    "Priority": 0,
    "CreationDate": "",
    "StartDate": "",
    "EndDate": "",
    "Files": [],
    "Images": []
  };
  constructor(public http: HttpClient) { }

  listContent() {
    let url = this.urlDomain + 'RetriveContents';
    let headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=UTF-8', 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT ,OPTIONS ' });
    return this.http.post(url, this.params, { headers });
  }

  addContent() {
    let url = this.urlDomain + 'AddContent';
    let headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=UTF-8', 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT ,OPTIONS ' });
    return this.http.post(url, this.params, { headers });
  }

  aboutContent() {
    let url = this.urlDomain + 'About';
    let headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=UTF-8', 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT ,OPTIONS ' });
    return this.http.post(url, this.params, { headers });
  }
}
