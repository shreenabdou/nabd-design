import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Md5 } from 'ts-md5/dist/md5';
import { AuthService } from 'src/app/services/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public lang;
  public contentLang;
  public errorFlag = false;
  public msgTxt = '';
  public form = new FormGroup({
    email: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });
  constructor(private translate: TranslateService, private authService: AuthService ,private router: Router) {

  }

  ngOnInit() {
    this.lang = this.translate.getDefaultLang();
    this.contentLang = this.lang[0].toUpperCase() + this.lang.slice(1);
  }

  login() {
    if (this.form.valid) {
      let md5 = new Md5;
      md5.appendStr(this.form.value.password);
      let key = md5.end();
      this.authService.params.Email = this.form.value.email;
      this.authService.params.Password = key.toString();
      this.authService.login().subscribe(res => {
        console.log(res);
        if (res['result']['status']) {
            localStorage.setItem('user' , res['result']['user']['firstName'] + ' ' + res['result']['user']['lastName']);
            localStorage.setItem('data' , res['result']['user']);
            this.router.navigateByUrl('/' + this.lang );
        }
        else {
          this.errorFlag = true;
          this.msgTxt = res['result']['message'+this.contentLang];
        }
      }, error => {
        this.errorFlag = true;
        this.msgTxt = "something wrong contact administrator";
      });
    }
  }

}
