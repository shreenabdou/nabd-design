import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})

export class SideMenuComponent implements OnInit {
  public lang;
  
  constructor(private translate: TranslateService) {
    
  }

  ngOnInit() {
    this.lang = this.translate.getDefaultLang();
  }

}
