import { Component, OnInit, Input, AfterViewInit, AfterViewChecked } from '@angular/core';
import { ContentService } from 'src/app/services/content.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-content-like-news',
  templateUrl: './content-like-news.component.html',
  styleUrls: ['./content-like-news.component.scss']
})
export class ContentLikeNewsComponent implements OnInit {
  @Input() contentList;
  @Input() page;
  public lang;
  public contentLang;

  constructor(private translate: TranslateService, private contentService: ContentService) { }

  ngOnInit() {
    this.lang = this.translate.getDefaultLang();
    this.contentLang = this.lang[0].toUpperCase() + this.lang.slice(1);

  }

  

}
