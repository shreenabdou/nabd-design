import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ContentService } from 'src/app/services/content.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-content-home',
  templateUrl: './content-home.component.html',
  styleUrls: ['./content-home.component.scss']
})
export class ContentHomeComponent implements OnInit {

  public lang;
  public contentLang;
  public cutLength = environment.cutLength;
  contentList = [];

  suggestionList = [];
  newsList = [];
  meetingList = [];
  activitesList = [];

  memberCount = 0;
  suggestionCount = 0;
  meetingCount = 0;
  circularsCount = 0;

  constructor(private translate: TranslateService, private contentService: ContentService) { }

  ngOnInit() {
    this.lang = this.translate.getDefaultLang();
    this.contentLang = this.lang[0].toUpperCase() + this.lang.slice(1);

  }

  ngAfterViewInit(): void {
    this.contentService.params.ContentTypes = ["Activities", "Meeting", "News"];
    this.contentService.listContent().subscribe(res => {
      if (res['result'] != undefined) {
        this.contentList = res['result'];
        this.contentList.forEach(content => {
          if (content['type'] == 'News')
            this.newsList.push(content);
          else if (content['type'] == 'Activities')
            this.activitesList.push(content);
          else if (content['type'] == 'Meeting')
            this.meetingList.push(content);
        });

      }
      console.log(res);
    });
  }

}
