import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { SharedModule } from "../shared/shared.module";
import { SuggestionComponent } from "../../pages/suggestion/suggestion.component";

const appRoutes: Routes = [
  { path: '', component: SuggestionComponent },
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(appRoutes),
  ],
  declarations: [
    SuggestionComponent
  ]
})
export class SuggestionModule { }
