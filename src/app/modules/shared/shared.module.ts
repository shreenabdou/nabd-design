import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { BootstrapModalModule } from 'ng6-bootstrap-modal';
import { NavComponent } from '../../components/nav/nav.component';
import { SideMenuComponent } from '../../components/side-menu/side-menu.component';
import { ContentService } from '../../services/content.service';
import { MeetingConferenceComponent } from '../../components/meeting-conference/meeting-conference.component';
import { CreateMeetingComponent } from '../../components/create-meeting/create-meeting.component';
import { ContentLikeNewsComponent } from '../../components/content-like-news/content-like-news.component';
import { CreateNewsComponent } from '../../components/create-news/create-news.component';
import { GeneralFunction } from 'src/app/GeneralFunction';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  imports: [
    CommonModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    HttpClientModule,
    RouterModule,
    BootstrapModalModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  declarations: [
    NavComponent,
    SideMenuComponent,
    MeetingConferenceComponent,
    CreateMeetingComponent,
    ContentLikeNewsComponent,
    CreateNewsComponent
  ],
  exports: [
    NavComponent,
    SideMenuComponent,
    ReactiveFormsModule,
    FormsModule,
    TranslateModule,
    BootstrapModalModule,
    MeetingConferenceComponent,
    CreateMeetingComponent,
    ContentLikeNewsComponent,
    CreateNewsComponent
  ],
  providers:[
    ContentService,
    GeneralFunction
  ],
  entryComponents:[
    CreateMeetingComponent,
    CreateNewsComponent
  ]
})
export class SharedModule { }
