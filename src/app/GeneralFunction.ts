import { FormGroup, FormControl } from "@angular/forms";
import { Injectable } from '@angular/core';


declare var $:any;
@Injectable({
    providedIn: 'root'
  })

export class GeneralFunction{
    handleOverLay(status , elementId){
        window.scrollTo(0, 0);
        console.log('overlay');
        if(status){
            $('#'+elementId).css('display','block');
        }
        else
        {
            $('#'+elementId).css('display','none');
        }
    }

    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
          let control = formGroup.get(field);
          if (control instanceof FormControl) {
            control.markAsTouched({ onlySelf: true });
          } else if (control instanceof FormGroup) {
            this.validateAllFormFields(control);
          }
        });
      }
}