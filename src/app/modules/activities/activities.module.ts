import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { SharedModule } from "../shared/shared.module";
import { ActivitiesComponent } from '../../pages/activities/activities.component';

const appRoutes: Routes = [
  { path: '', component: ActivitiesComponent }
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(appRoutes),
  ],
  declarations: [ActivitiesComponent]
})
export class ActivitiesModule { }
