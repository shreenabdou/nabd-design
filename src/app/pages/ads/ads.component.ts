import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-ads',
  templateUrl: './ads.component.html',
  styleUrls: ['./ads.component.scss']
})
export class AdsComponent implements OnInit {

  public lang;
  constructor(private translate: TranslateService) {
    
  }

  ngOnInit() {
    this.lang = this.translate.getDefaultLang();
  }

}
