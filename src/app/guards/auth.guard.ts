import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanLoad, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad {

  constructor(private authService: AuthService, private router: Router) { }

  canLoad(route: import("@angular/router").Route): boolean | Observable<boolean> | Promise<boolean> {
    if (this.authService.isLoggedIn()) {
      console.log('logged activate');
      return true;
    } else {
      this.router.navigate([environment.lang + "/login"]);
      console.log('not logged activate');
      return false;
    }
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.authService.isLoggedIn()) {
      console.log('logged activate');
      return true;
    } else {
      this.router.navigate([environment.lang + "/login"]);
      console.log('not logged activate');
      return false;
    }
  }
}
