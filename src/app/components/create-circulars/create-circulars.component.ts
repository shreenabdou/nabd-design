import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-create-circulars',
  templateUrl: './create-circulars.component.html',
  styleUrls: ['./create-circulars.component.scss']
})
export class CreateCircularsComponent implements OnInit {

  public lang;
  constructor(private translate: TranslateService) {
    
  }

  ngOnInit() {
    this.lang = this.translate.getDefaultLang();
  }

}
