import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { SharedModule } from "../shared/shared.module";
import { ConferenceComponent } from '../../pages/conference/conference.component';

const appRoutes: Routes = [
  { path: '', component: ConferenceComponent },
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(appRoutes),
  ],
  declarations: [ConferenceComponent]
})
export class ConferenceModule { }
