import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { SharedModule } from "../shared/shared.module";
import { CircularsComponent } from '../../pages/circulars/circulars.component';
import { CreateCircularsComponent } from '../../components/create-circulars/create-circulars.component';

const appRoutes: Routes = [
  { path: '', component: CircularsComponent },
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(appRoutes),
  ],
  declarations: [
    CircularsComponent,
    CreateCircularsComponent
  ]
})
export class CircularsModule { }
