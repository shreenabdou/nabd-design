import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { SharedModule } from "../shared/shared.module";
import { ReportComponent } from '../../pages/report/report.component';

const appRoutes: Routes = [
  { path: '', component: ReportComponent },
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(appRoutes),
  ],
  declarations: [ReportComponent]
})
export class ReportModule { }
