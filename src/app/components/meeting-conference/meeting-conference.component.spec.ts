import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeetingConferenceComponent } from './meeting-conference.component';

describe('MeetingConferenceComponent', () => {
  let component: MeetingConferenceComponent;
  let fixture: ComponentFixture<MeetingConferenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeetingConferenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetingConferenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
