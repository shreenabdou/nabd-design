import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { DialogComponent, DialogService } from "ng6-bootstrap-modal";
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { GeneralFunction } from '../../GeneralFunction';
import { ContentService } from 'src/app/services/content.service';

export interface CreateMeetingModal {
  page: string
  lang: string,
}

@Component({
  selector: 'app-create-meeting',
  templateUrl: './create-meeting.component.html',
  styleUrls: ['./create-meeting.component.scss']
})
export class CreateMeetingComponent extends DialogComponent<CreateMeetingModal, boolean> implements CreateMeetingModal, OnInit {

  public lang;
  public page;
  public form = new FormGroup({
    titleAr: new FormControl('', [Validators.required]),
    bodyAr: new FormControl('', [Validators.required]),
    titleEn: new FormControl('', [Validators.required]),
    bodyEn: new FormControl('', [Validators.required]),
    startDate: new FormControl('', [Validators.required]),
    endDate: new FormControl('', [Validators.required]),
    startTime: new FormControl('', [Validators.required]),
    endTime: new FormControl('', [Validators.required]),
    active: new FormControl(''),
    access: new FormControl(''),
    multiImages: new FormControl('')
  });
  private multiImagesString: Array<string> = [];
  public tempImageType = [];

  constructor(dialogService: DialogService, private translate: TranslateService, private generalFunc: GeneralFunction, private contentService: ContentService) {
    super(dialogService);
  }

  ngOnInit() {
    this.lang = this.translate.getDefaultLang();
  }

  createMeeting() {
    this.result = true;
    this.close();
  }

  create() {
    console.log(this.form);
    let startMeeting = this.form.value.startDate + ' ' + this.form.value.startTime;
    let endMeeting = this.form.value.endDate + ' ' + this.form.value.endTime;
    if (this.form.valid) {
      this.contentService.params.BodyAr = this.form.value.bodyAr;
      this.contentService.params.TitleAr = this.form.value.titleAr;
      this.contentService.params.BodyEn = this.form.value.bodyEn;
      this.contentService.params.TitleEn = this.form.value.titleEn;
      this.contentService.params.Active = this.form.value.active;
      this.contentService.params.Acsess = this.form.value.access;
      this.contentService.params.StartDate = startMeeting;
      this.contentService.params.EndDate = endMeeting;
      this.contentService.params.Type = this.page;
      this.contentService.params.Images = this.multiImagesString;
      console.log(this.contentService.params);
      this.contentService.addContent().subscribe(res => {
        console.log(res);
        if(res['result']['status']){
          this.result = true;
        }
      });
    }
    else {
      this.generalFunc.validateAllFormFields(this.form);
    }

  }

  handleFileSelect(evt) {
    var files = evt.target.files;
    // this.source = sourceName;
    this.tempImageType = [];
    // var file = files[0];
    // if (this.source == 'multiDealImages') {
    this.multiImagesString = [];
    document.getElementById('previewImg').innerHTML = '';
    // }
    for (let i = 0; i < files.length; i++) {
      if (files && files[i]) {
        var reader = new FileReader();
        // if (this.source == 'multiDealImages') {
        this.tempImageType[i] = "data:" + files[i]['type'] + ";base64,";
        // }
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsBinaryString(files[i]);
      }
    }

  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.multiImagesString.push(btoa(binaryString));
    document.getElementById('previewImg').innerHTML += '<img style="width:150px ; height:150px ; margin:10px 5px 0px 5px" src="' + this.tempImageType[this.tempImageType.length - 1] + this.multiImagesString[this.multiImagesString.length - 1] + '"/>';

  }
}
