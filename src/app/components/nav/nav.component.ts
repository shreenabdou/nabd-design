import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  public lang;
  public reverseLang = '';
  public userName = '';
  constructor(private translate: TranslateService , private router: Router) {
    
  }

  ngOnInit() {
    this.userName = localStorage.getItem('user');
    this.lang = this.translate.getDefaultLang();
    if(this.lang == 'ar')
      this.reverseLang = 'en';
    else if(this.lang == 'en')
      this.reverseLang = 'ar';
  }

  logout(){
    localStorage.clear();
    this.router.navigate([environment.lang + "/login"]);
  }
}
