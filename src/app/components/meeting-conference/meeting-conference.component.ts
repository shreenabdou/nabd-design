import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ContentService } from 'src/app/services/content.service';

@Component({
  selector: 'app-meeting-conference',
  templateUrl: './meeting-conference.component.html',
  styleUrls: ['./meeting-conference.component.scss']
})
export class MeetingConferenceComponent implements OnInit {
  
  @Input() page;
  public lang;
  public contentLang;
  @Input() contentList;

  constructor(private translate: TranslateService,private contentService: ContentService) { }

  ngOnInit() {
    this.lang = this.translate.getDefaultLang();
    this.contentLang = this.lang[0].toUpperCase() + this.lang.slice(1);
    
  }

  

}
