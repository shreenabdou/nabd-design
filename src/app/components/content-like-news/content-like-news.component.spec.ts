import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentLikeNewsComponent } from './content-like-news.component';

describe('ContentLikeNewsComponent', () => {
  let component: ContentLikeNewsComponent;
  let fixture: ComponentFixture<ContentLikeNewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentLikeNewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentLikeNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
