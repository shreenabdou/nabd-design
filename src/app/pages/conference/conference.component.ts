import { Component, OnInit, AfterViewInit } from '@angular/core';
import { CreateMeetingComponent } from 'src/app/components/create-meeting/create-meeting.component';
import { DialogService } from 'ng6-bootstrap-modal';
import { TranslateService } from '@ngx-translate/core';
import { ContentService } from 'src/app/services/content.service';

@Component({
  selector: 'app-conference',
  templateUrl: './conference.component.html',
  styleUrls: ['./conference.component.scss']
})
export class ConferenceComponent implements OnInit , AfterViewInit{

  public lang;
  public pageOwner = 'Conference';
  public contentLang;
  contentList = [];
  constructor(private dialogService: DialogService, private translate: TranslateService, private contentService: ContentService) {

  }

  ngOnInit() {
    this.lang = this.translate.getDefaultLang();
  }

  create() {
    document.getElementById('body').style.overflow = "hidden";
    let disposable = this.dialogService.addDialog(CreateMeetingComponent,
      { lang: this.lang , page:this.pageOwner}, { backdropColor: 'rgba(0, 0, 0, 0.5)' }).subscribe((isConfirmed) => {
        console.log(isConfirmed);
        if (isConfirmed) {
          console.log(isConfirmed['result']['content']);
          this.contentList.splice(0,0,isConfirmed['result']['content']);
          document.getElementById('body').style.overflow = 'auto';
          console.log(this.contentList);
        }
        else {
          document.getElementById('body'). style.overflow = 'auto';
        }
      });
  }

  ngAfterViewInit(): void {
    console.log(this.contentLang);
    this.contentService.params.ContentTypes = [this.pageOwner];
    console.log(this.pageOwner);
    console.log(this.contentService.params);
    this.contentService.listContent().subscribe(res => {
      if (res['result'] != undefined) {
        this.contentList = res['result'];
      }
      console.log(res);
    });
  }
}
