import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { SharedModule } from "../shared/shared.module";
import { ChatComponent } from "../../pages/chat/chat.component";

const appRoutes: Routes = [
  { path: '', component: ChatComponent },
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(appRoutes),
  ],
  declarations: [
    ChatComponent
  ]
})
export class ChatModule { }
