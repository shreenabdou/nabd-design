import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private urlDomain = environment.apiURI;
  public params = {
    "Password":"",
	  "Email":""
  };
  constructor(public http: HttpClient) { }

  login() {
    let url = this.urlDomain + 'Login';
    let headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=UTF-8','Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT ,OPTIONS ' });
    return this.http.post(url, this.params, { headers });
  }

  isLoggedIn(){
    let userObj = localStorage.getItem('user');
    if(userObj){
        return true;
    }
    else{
      return false;
    }
  }
}
