import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ContentService } from 'src/app/services/content.service';

@Component({
  selector: 'app-circulars',
  templateUrl: './circulars.component.html',
  styleUrls: ['./circulars.component.scss']
})
export class CircularsComponent implements OnInit {

  public lang;
  public contentLang;
  contentList = [];
  constructor(private translate: TranslateService,private contentService: ContentService) { }

  ngOnInit() {
    this.lang = this.translate.getDefaultLang();
    this.contentLang = this.lang[0].toUpperCase() + this.lang.slice(1);
    console.log(this.contentLang);
    this.contentService.params.ContentTypes = ["Circular"];
    this.contentService.listContent().subscribe(res => {
      if (res['result'] != undefined) {
        this.contentList = res['result'];
      }
      console.log(res);
    });
  }

}
