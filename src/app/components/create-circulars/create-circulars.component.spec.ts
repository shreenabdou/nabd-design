import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCircularsComponent } from './create-circulars.component';

describe('CreateCircularsComponent', () => {
  let component: CreateCircularsComponent;
  let fixture: ComponentFixture<CreateCircularsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateCircularsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCircularsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
