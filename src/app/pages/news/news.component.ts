import { Component, OnInit, AfterViewInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';
import { ContentService } from 'src/app/services/content.service';
import { DialogService } from 'ng6-bootstrap-modal';
import { CreateNewsComponent } from 'src/app/components/create-news/create-news.component';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit , AfterViewInit{

  public lang;
  public contentLang;
  public pageOwner = 'News';
  contentList = [];
  constructor(private dialogService: DialogService, private translate: TranslateService, private contentService: ContentService) {

  }

  ngOnInit() {
    this.lang = this.translate.getDefaultLang();
    this.contentLang = this.lang[0].toUpperCase() + this.lang.slice(1);
  }

  create() {
    document.getElementById('body').style.overflow = "hidden";
    let disposable = this.dialogService.addDialog(CreateNewsComponent,
      { lang: this.lang, page: this.pageOwner }, { backdropColor: 'rgba(0, 0, 0, 0.5)' }).subscribe((isConfirmed) => {
        //We get dialog result
        if (isConfirmed) {
          console.log(isConfirmed['result']['content']);
          this.contentList.splice(0,0,isConfirmed['result']['content']);
          document.getElementById('body').style.overflow = 'auto';
          console.log(this.contentList);
        }
        else {
          document.getElementById('body'). style.overflow = 'auto';
        }
      });
  }

  ngAfterViewInit(): void {
    this.contentService.params.ContentTypes = ["News"];
    this.contentService.listContent().subscribe(res => {
      if (res['result'] != undefined) {
        this.contentList = res['result'];
      }
      console.log(res);
    });
  }
}
