import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { SharedModule } from "../shared/shared.module";
import { MeetingComponent } from '../../pages/meeting/meeting.component';



const appRoutes: Routes = [
  { path: '', component: MeetingComponent },
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(appRoutes),
  ],
  declarations: [
    MeetingComponent,
  ],
  
})
export class MeetingModule { }
