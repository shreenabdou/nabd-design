import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-suggestion',
  templateUrl: './suggestion.component.html',
  styleUrls: ['./suggestion.component.scss']
})
export class SuggestionComponent implements OnInit {

  public lang;
  constructor(private translate: TranslateService) {
    
  }

  ngOnInit() {
    this.lang = this.translate.getDefaultLang();
  }

}
