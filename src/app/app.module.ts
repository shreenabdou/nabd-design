import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from './modules/shared/shared.module';
import { ContentHomeComponent } from '../app/pages/content-home/content-home.component';
import { LoginComponent } from '../app/pages/login/login.component';
import { AboutUsComponent } from '../app/pages/about-us/about-us.component';
import { AuthGuard } from './guards/auth.guard';


export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

const appRoutes = [
  {
    path: 'en',
    children: [
      { path: '', component: ContentHomeComponent, canActivate: [AuthGuard] },
      { path: 'about-us', component: AboutUsComponent, canActivate: [AuthGuard] },
      { path: 'login', component: LoginComponent },
      { path: 'news', loadChildren: "./modules/news/news.module#NewsModule", canLoad: [AuthGuard] },
      { path: 'meeting', loadChildren: "./modules/meeting/meeting.module#MeetingModule", canLoad: [AuthGuard] },
      { path: 'invitations', loadChildren: "./modules/invitation/invitation.module#InvitationModule", canLoad: [AuthGuard] },
      { path: 'conferences', loadChildren: "./modules/conference/conference.module#ConferenceModule", canLoad: [AuthGuard] },
      { path: 'member', loadChildren: "./modules/member/member.module#MemberModule", canLoad: [AuthGuard] },
      { path: 'activities', loadChildren: "./modules/activities/activities.module#ActivitiesModule", canLoad: [AuthGuard] },
      { path: 'suggestion', loadChildren: "./modules/suggestion/suggestion.module#SuggestionModule", canLoad: [AuthGuard] },
      { path: 'chat', loadChildren: "./modules/chat/chat.module#ChatModule", canLoad: [AuthGuard] },
      { path: 'ads', loadChildren: "./modules/ads/ads.module#AdsModule", canLoad: [AuthGuard] },
      { path: 'reports', loadChildren: "./modules/report/report.module#ReportModule", canLoad: [AuthGuard] },
      { path: 'circulars', loadChildren: "./modules/circulars/circulars.module#CircularsModule", canLoad: [AuthGuard] }
    ]
  },
  {
    path: 'ar',
    children: [
      { path: '', component: ContentHomeComponent, canActivate: [AuthGuard] },
      { path: 'about-us', component: AboutUsComponent, canActivate: [AuthGuard] },
      { path: 'login', component: LoginComponent },
      { path: 'news', loadChildren: "./modules/news/news.module#NewsModule", canLoad: [AuthGuard] },
      { path: 'meeting', loadChildren: "./modules/meeting/meeting.module#MeetingModule", canLoad: [AuthGuard] },
      { path: 'invitations', loadChildren: "./modules/invitation/invitation.module#InvitationModule", canLoad: [AuthGuard] },
      { path: 'conferences', loadChildren: "./modules/conference/conference.module#ConferenceModule", canLoad: [AuthGuard] },
      { path: 'member', loadChildren: "./modules/member/member.module#MemberModule", canLoad: [AuthGuard] },
      { path: 'activities', loadChildren: "./modules/activities/activities.module#ActivitiesModule", canLoad: [AuthGuard] },
      { path: 'suggestion', loadChildren: "./modules/suggestion/suggestion.module#SuggestionModule", canLoad: [AuthGuard] },
      { path: 'chat', loadChildren: "./modules/chat/chat.module#ChatModule", canLoad: [AuthGuard] },
      { path: 'ads', loadChildren: "./modules/ads/ads.module#AdsModule", canLoad: [AuthGuard] },
      { path: 'reports', loadChildren: "./modules/report/report.module#ReportModule", canLoad: [AuthGuard] },
      { path: 'circulars', loadChildren: "./modules/circulars/circulars.module#CircularsModule", canLoad: [AuthGuard] }
    ]
  },
  {
    path: '',
    redirectTo: 'ar',
    pathMatch: 'full'
  }

];

@NgModule({
  declarations: [
    AppComponent,
    ContentHomeComponent,
    LoginComponent,
    AboutUsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [
    RouterModule
  ],
  entryComponents: [
  ]
})
export class AppModule { }
