import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { SharedModule } from "../shared/shared.module";
import { AdsComponent } from '../../pages/ads/ads.component';

const appRoutes: Routes = [
  { path: '', component: AdsComponent },
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(appRoutes),
  ],
  declarations: [
    AdsComponent
  ]
})
export class AdsModule { }
