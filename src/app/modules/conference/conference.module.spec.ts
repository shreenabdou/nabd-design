import { ConferenceModule } from './conference.module';

describe('ConferenceModule', () => {
  let conferenceModule: ConferenceModule;

  beforeEach(() => {
    conferenceModule = new ConferenceModule();
  });

  it('should create an instance', () => {
    expect(conferenceModule).toBeTruthy();
  });
});
