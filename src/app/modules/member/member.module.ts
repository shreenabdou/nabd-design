import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { SharedModule } from "../shared/shared.module";
import { MemberComponent } from "../../pages/member/member.component";

const appRoutes: Routes = [
  { path: '', component: MemberComponent },
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(appRoutes),
  ],
  declarations: [
    MemberComponent
  ]
})
export class MemberModule { }
