import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { SharedModule } from "../shared/shared.module";
import { NewsComponent } from '../../pages/news/news.component';


const appRoutes: Routes = [
  { path: '', component: NewsComponent },
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(appRoutes),
  ],
  declarations: [
    NewsComponent,
    
  ],
  entryComponents: [
  ]
})
export class NewsModule { }
