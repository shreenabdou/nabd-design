import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';

declare var $: any;
const arr = window.location.pathname.split('/');
export function getStyle() {
  if (arr[1] == 'ar') {
    return ['../assets/ar/scss/ar-style.scss']
  }
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: getStyle(),
})
export class AppComponent {
  title = 'nabd';
  constructor(private translate: TranslateService) {
    if (arr[1] == 'ar' || arr[1] == 'en') {
      environment.lang = arr[1];
      translate.setDefaultLang(arr[1]);
    }
    else {
      environment.lang = 'ar';
      translate.setDefaultLang('ar');
    }

  }
}
