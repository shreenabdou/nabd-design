import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.scss']
})
export class MemberComponent implements OnInit {

  public lang;
  constructor(private translate: TranslateService) {
    
  }

  ngOnInit() {
    this.lang = this.translate.getDefaultLang();
  }

}
