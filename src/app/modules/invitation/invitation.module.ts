import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { SharedModule } from "../shared/shared.module";
import { InvitationComponent } from '../../pages/invitation/invitation.component';

const appRoutes: Routes = [
  { path: '', component: InvitationComponent },
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(appRoutes),
  ],
  declarations: [InvitationComponent]
})
export class InvitationModule { }
