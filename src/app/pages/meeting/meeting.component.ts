import { Component, OnInit, AfterViewInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { DialogService } from 'ng6-bootstrap-modal';
import { CreateMeetingComponent } from 'src/app/components/create-meeting/create-meeting.component';
import { ContentService } from 'src/app/services/content.service';

@Component({
  selector: 'app-meeting',
  templateUrl: './meeting.component.html',
  styleUrls: ['./meeting.component.scss']
})
export class MeetingComponent implements OnInit , AfterViewInit {

  public lang;
  public pageOwner = 'Meeting';
  public contentLang;
  contentList = [];
  constructor(private dialogService: DialogService, private translate: TranslateService, private contentService: ContentService) {

  }

  ngOnInit() {
    this.lang = this.translate.getDefaultLang();
    this.contentLang = this.lang[0].toUpperCase() + this.lang.slice(1);
  }

  create() {
    document.getElementById('body').style.overflow = "hidden";
    let disposable = this.dialogService.addDialog(CreateMeetingComponent,
      { lang: this.lang, page: this.pageOwner }, { backdropColor: 'rgba(0, 0, 0, 0.5)' }).subscribe((isConfirmed) => {
        console.log(isConfirmed);
        //We get dialog result
        if (isConfirmed) {
          console.log(isConfirmed['result']['content']);
          this.contentList.splice(0,0,isConfirmed['result']['content']);
          document.getElementById('body').style.overflow = 'auto';
          console.log(this.contentList);
        }
        else {
          document.getElementById('body'). style.overflow = 'auto';
        }
      });
  }

  ngAfterViewInit(): void {
    console.log(this.contentLang);
    this.contentService.params.ContentTypes = [this.pageOwner];
    console.log(this.pageOwner);
    console.log(this.contentService.params);
    this.contentService.listContent().subscribe(res => {
      if (res['result'] != undefined) {
        this.contentList = res['result'];
      }
      console.log(res);
    });
  }
}
