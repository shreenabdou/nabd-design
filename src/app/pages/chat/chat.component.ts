import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

  public lang;
  constructor(private translate: TranslateService) {
    
  }

  ngOnInit() {
    this.lang = this.translate.getDefaultLang();
  }

}
